﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using ProgressBar;

public class RitualStation : MonoBehaviour {
	public string stationName;
	public string ritualName;
	public string ritualActionName;

	public float ritualCompletionTime;
	public float ritualValue;

	float ritualTimeLeft;

	public RitualManager ritualManager;

	// The game object performing the ritual
	public IRitualPerformer ritualPerformer;
	public bool isPerformingRitual;

	[SerializeField]
	public ProgressBarBehaviour progressBar;

	[SerializeField]
	CanvasGroup progressBarGroup;

	[SerializeField]
	Transform performPosition;

	[SerializeField]
	AudioClip[] audioClips;
	AudioSource[] audioSources;

	[SerializeField]
	float audioStartPoint;

	void Awake()
	{
		if (audioClips.Length > 0) {
			audioSources = new AudioSource[audioClips.Length];
			for (int i = 0; i < audioClips.Length; i++) {
				AudioSource newAudio = gameObject.AddComponent<AudioSource>();

				newAudio.clip = audioClips [i];
				newAudio.loop = true;
				newAudio.playOnAwake = false;
				newAudio.volume = 0.5f; 

				audioSources [i] = newAudio;
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (!ritualManager) {
			Debug.LogError ("Ritual station: '" + stationName + "' has no ritual manager");
			return;
		}
		
		ritualManager.SetRitual (this);
	}

	void OnTriggerExit2D(Collider2D collider)
	{
		ritualManager.SetRitual (null);
	}

	void Update()
	{
		if (isPerformingRitual) {
			if (ritualTimeLeft > 0.0f) {
				ritualTimeLeft -= Time.deltaTime;

				// Update progress bar percentage
				float percent = 100.0f - (ritualTimeLeft / ritualCompletionTime * 100.0f);
				progressBar.Value = percent;
			} else {
				EndRitual ();
			}
		}
	}

	public void StartRitual(IRitualPerformer performer)
	{
		ritualPerformer = performer;
		ritualTimeLeft = ritualCompletionTime;
		isPerformingRitual = true;
		showProgressBar (true);

		ritualPerformer.OnRitualDidStart (performPosition);

		// HACK: 1 hour before submission, cleanup later
		ritualManager.HideRitualMarker ();

		startAudio ();
	}

	void EndRitual()
	{
		// Calculate ritual power amount - full if target ritual, else 1/4 power
		float finalValue = ritualManager.targetRitual == this ? ritualValue : ritualValue/4.0f;

		ritualPerformer.OnRitualDidEnd (performPosition, finalValue);
		ritualPerformer = null;
		isPerformingRitual = false;
		showProgressBar (false);

		// Reset the progressbar value to 0 for sanity, maybe not required
		progressBar.Value = 0.0f;

		ritualManager.ritualComplete (this);

		endAudio ();
	}

	void showProgressBar(bool show)
	{
		progressBarGroup.alpha = show ? 1.0f : 0.0f;
	}

	void startAudio()
	{
		foreach (AudioSource audio in audioSources) {
			if (audioStartPoint > 0) {
				audio.time = audioStartPoint;
			}

			audio.Play ();
		}
	}

	void endAudio()
	{
		foreach (AudioSource audio in audioSources) {
			audio.Stop ();
		}
	}
}
