﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class ButtonManager : MonoBehaviour
{

    [SerializeField]
    GameObject yButton;

    [SerializeField]
    GameObject xButton;

    [SerializeField]
    GameObject aButton;

    [SerializeField]
    GameObject bButton;


    void Update()
    {

        if (Input.GetButtonDown("Action"))
        {
            if (xButton)
            {
                ExecuteEvents.Execute<ISubmitHandler>(xButton, null, (z, y) => z.OnSubmit(y));
            }
        }


        if (Input.GetButtonDown("Teleport"))
        {
            if (yButton)
            {
                ExecuteEvents.Execute<ISubmitHandler>(yButton, null, (z, y) => z.OnSubmit(y));
            }
        }

        if (Input.GetButtonDown("Attack"))
        {
            if (bButton)
            {
                ExecuteEvents.Execute<ISubmitHandler>(bButton, null, (z, y) => z.OnSubmit(y));
            }
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (aButton)
            {
                ExecuteEvents.Execute<ISubmitHandler>(aButton, null, (z, y) => z.OnSubmit(y));
            }
        }
    }
}
