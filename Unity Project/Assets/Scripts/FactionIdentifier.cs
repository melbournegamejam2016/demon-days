﻿using UnityEngine;
using System.Collections;

public class FactionIdentifier : MonoBehaviour {

    public Faction faction;

}

[System.Serializable]
public struct Faction
{
    public int identifier;
    public Color colour;
}
