﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class EnergyManager : MonoBehaviour, IEnergyDamage {

    [SerializeField]
    float maxEnergy = 100;
    [SerializeField]
    float lossPerSecond = 5;
    [SerializeField]
    Text energyUIText;
    [SerializeField]
    float startEnergy = 50;
    [SerializeField]
    PointsMover enegryPrefab;

    public float energy { get; private set; }

    public bool canTeleport {
        get
        {
            return energy > 0;
        }
    }

    void Awake()
    {
        energy = startEnergy;
    }

    [SerializeField]
    GameObject ritualManager;

    public void AddEnergy (float amount)
    {
        Vector3 position = Vector3.zero;
        if (amount < 0)
        {
            position = ritualManager.transform.position;
        }
        else
        {
            position = Camera.main.WorldToScreenPoint(transform.position);
        }
        PointsMover clone = (PointsMover)Instantiate(enegryPrefab, position, Quaternion.identity);
        clone.AssignPoints((int)amount, () => energy = Mathf.Clamp(energy + amount, 0, maxEnergy));
    }

    public void RemoveAllEnergy()
    {
        PointsMover clone = (PointsMover)Instantiate(enegryPrefab, Camera.main.WorldToScreenPoint(transform.position), Quaternion.identity);
        clone.AssignPoints(-(int)energy, () => { return; });
        energy = 0;
    }

    public void EnergyUpdate()
    {
        energy -= lossPerSecond * Time.deltaTime;
    }

    void Update()
    {
        energyUIText.text = energy.ToString("F0") + "%";
    }
}
