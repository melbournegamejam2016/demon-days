﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityStandardAssets._2D;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof (Platformer2DUserControl))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(EnergyManager))]

public class DevilMovement: MonoBehaviour, IRitualPerformer {
	[SerializeField]
	GameManager gameManager;

	[SerializeField]
	Collider2D topGround;

	[SerializeField]
	Collider2D belowGround;

	[SerializeField]
	float attackRadius = 2.5f;

	[SerializeField]
	LayerMask civillianMask;

	[SerializeField]
	public bool isBelowGround = true;

	Platformer2DUserControl userControl;
    Animator animator;

    [SerializeField]
    AudioSource classicalDemonRunnerLoop;

    [SerializeField]
    AudioSource metalDemonRunnerLoop;

	// Ritual Related
	[SerializeField]
	public RitualStation activeRitual = null;
	float ritualTimeLeft;
	bool shouldFlipAtRitualEnd;

	[SerializeField]
	NoiseAndScratches noiseAndScratchesEffect;

	[SerializeField]
	Grayscale grayScaleEffect;

	[SerializeField]
	VignetteAndChromaticAberration vignetteEffect;

	// Enable crazy image effects when teleporting above ground
	[SerializeField]
	bool enableCrazy;

	[SerializeField]
	float crazyGrowthFactor = 1.5f;

	[SerializeField]
	GameObject actionMarker;

    // Energy Related
    EnergyManager energyManager;


	RigidbodyConstraints2D originalConstraints;

	void Awake()
	{
        energyManager = GetComponent<EnergyManager>();
        animator = GetComponent<Animator>();
		userControl = GetComponent<Platformer2DUserControl> ();
		originalConstraints = GetComponent<Rigidbody2D>().constraints;
	}

    void Start ()
    {
        metalDemonRunnerLoop.Pause();
    }

	// Update is called once per frame
	void Update () {
        actionMarker.SetActive (!isBelowGround && isNearSummoners ());

		if (gameManager.isGameOver) {
			// Quick and dirty - this will be called every frame during the game over screen
			SetUserControlEnabled(isBelowGround);
			return;
		}

		if (activeRitual && activeRitual.isPerformingRitual) {
			// Disable actions when performing rituals
			return;
		}

        // Can only teleport up if energy is available;
		if (Input.GetButtonDown("Teleport")) {
            if (energyManager.canTeleport || !isBelowGround)
            {
                DoTeleport();
            } else
            {
                //TODO: Display a bif message that says out of energy
            }
		}

		if (Input.GetButtonDown("Attack")){
			DoAttack ();
		}

        if (Input.GetButtonDown("Action"))
        {
			DoAction ();
        }

		if (!isBelowGround) {
			energyManager.EnergyUpdate ();
		}

        // Banish the demon to hell if he runs out of energy
        if(!isBelowGround && !energyManager.canTeleport)
        {
            DoTeleport();
        }

	}

	void DoTeleport()
	{
		Vector2 teleportDestination;

		Collider2D destCollider = isBelowGround ? topGround : belowGround;

		teleportDestination.x = transform.position.x;
		teleportDestination.y = destCollider.bounds.max.y;

        // Animate the devil
        animator.SetTrigger(isBelowGround ? "Teleport up" : "Teleport down");

        // Move devil and update isBelowGround flag
        transform.position = teleportDestination;
		isBelowGround = !isBelowGround;

        // Set the new audio
        if (isBelowGround)
        {
            classicalDemonRunnerLoop.UnPause();
            metalDemonRunnerLoop.Pause();
			if (noiseAndScratchesEffect && grayScaleEffect && enableCrazy) {
				noiseAndScratchesEffect.enabled = false;
				grayScaleEffect.enabled = false;
				vignetteEffect.intensity = .02f;
				transform.localScale *= (1/Mathf.Max(0.1f, crazyGrowthFactor));
			}
        }
        else
        {
            classicalDemonRunnerLoop.Pause();
            metalDemonRunnerLoop.UnPause();

			if (noiseAndScratchesEffect && grayScaleEffect && enableCrazy) {
				noiseAndScratchesEffect.enabled = true;
				grayScaleEffect.enabled = true;
				vignetteEffect.intensity = .3f;
				transform.localScale *= Mathf.Max(0.1f, crazyGrowthFactor);
			}
        }
	}
		
	void DoAttack()
	{
        animator.SetTrigger("Attack");
	}

	// Performs raycast and applies damage - called from attack animation via event
	public void DoAttack_Damage()
	{
		RaycastHit2D hit;

		if (hit = Physics2D.CircleCast (transform.position, attackRadius, transform.right, attackRadius, civillianMask)) {
			ExecuteEvents.Execute<IDamagable> (hit.collider.gameObject, null, (x, y) => x.DoDamage(100.0f));
		}
	}

	void DoAction()
	{
		if (activeRitual) {
			DoRitual ();
		} 
		else {
			AcceptMission();
		}
	}

	void DoRitual()
	{
		activeRitual.StartRitual (this);
	}
    
	void SetUserControlEnabled(bool enabled)
	{
		GetComponent<Rigidbody2D> ().constraints = enabled ? originalConstraints : RigidbodyConstraints2D.FreezePositionX;
		userControl.enabled = enabled;
	}

    void AcceptMission()
    {
        RaycastHit2D[] hits;

        Debug.Log("Accept mission");

        hits = Physics2D.CircleCastAll(transform.position, attackRadius, Vector2.up, attackRadius, civillianMask);
        foreach (RaycastHit2D hit in hits)
        {
            ExecuteEvents.Execute<IAcceptMissions>(hit.collider.gameObject, null, (x, y) => x.AcceptMission());
        }        
    }

	void OnDrawGizmos()
	{
		// Attack Radius Gizmo
		Gizmos.DrawWireSphere (transform.position, attackRadius);
	}

	public void OnRitualDidStart(Transform performPosition) {
		animator.SetTrigger ("RitualStart");

		transform.position = new Vector3(performPosition.position.x, transform.position.y, performPosition.position.z);

		// Flip devil to face the correct position, if not facing already
		if (performPosition.localScale.x < 0.0f && transform.localScale.x > 0.0f
			|| performPosition.localScale.x > 0.0f && transform.localScale.x < 0.0f) {
			Flip ();

			// Remember to flip the devil back at the end
			shouldFlipAtRitualEnd = true;
		}

		// Disable movement and controls
		SetUserControlEnabled (false);
	}

	public void OnRitualDidEnd(Transform performPosition, float ritualValue) {
		if (shouldFlipAtRitualEnd) {
			Flip ();
			shouldFlipAtRitualEnd = false;
		}

		// Add ritual energy
		energyManager.AddEnergy(ritualValue);

		animator.SetTrigger ("RitualEnd");

		// Enable movement and controls
		SetUserControlEnabled (true);
	}

	void Flip()
	{
		Vector3 scale = transform.localScale;
		scale.x *= -1.0f;
		transform.localScale = scale;
	}

	public void AddRitualEnergy(float energy)
	{
		energyManager.AddEnergy (energy);	
	}

	public bool isNearSummoners()
	{
		RaycastHit2D[] hits;

		hits = Physics2D.CircleCastAll(transform.position, attackRadius, Vector2.up, attackRadius, civillianMask);
		foreach (RaycastHit2D hit in hits)
		{
			People.MissionProvider provider = hit.collider.gameObject.GetComponent<People.MissionProvider> ();
			if (provider && provider.hasMission ()) {
				return true;
			}
			//ExecuteEvents.Execute<IAcceptMissions>(hit.collider.gameObject, null, (x, y) => x.AcceptMission());
		}

		return false;
	}
}
