﻿using UnityEngine;
using System.Collections;

namespace People
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Motor : MonoBehaviour, IKillable
    {

        float currentTarget = 0;
        [SerializeField]
        float minX = -1;
        [SerializeField]
        float maxX = 1;
        Rigidbody2D myRigidBody;
        [SerializeField]
        float speed = 1;
        float currentSpeed;

        void Start()
        {
            currentSpeed = speed;
            myRigidBody = GetComponent<Rigidbody2D>();
            StartCoroutine(MoveToTarget());
        }

        public void Stop()
        {
            currentSpeed = 0;
        }

        public void Resume()
        {
            currentSpeed = speed;
        }

        IEnumerator MoveToTarget()
        {
            YieldInstruction waitForFixed = new WaitForFixedUpdate();
            while (true)
            {
                // Set a new target
                currentTarget = Random.Range(minX, maxX);

                // Flip the sprite
                if (currentTarget - transform.position.x > 0)
                {
                    transform.localScale = new Vector3(1, 1, 1);
                }
                else
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                }

                // Move the sprite
                while (Mathf.Abs(currentTarget - transform.position.x) > 0.1f)
                {
                    myRigidBody.MovePosition((Vector2)transform.position + new Vector2(currentTarget - transform.position.x, 0).normalized * currentSpeed * Time.deltaTime);
                    yield return waitForFixed;
                }
                yield return waitForFixed;
            }
        }

        public void IsKilled()
        {
            StopAllCoroutines();
        }
    }
}