﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace People
{
    [RequireComponent(typeof (Animator))]
    public class MissionProvider : MonoBehaviour, IKillable, IAcceptMissions
    {

        [SerializeField]
        float minWaitTime;
        [SerializeField]
        float maxWaitTime;
        [SerializeField]
        GameObject missionText;
        [SerializeField]
        float missionDisplayTime;
        Motor motor;
        [SerializeField]
        GameObject targetDisplay;
        Animator animator;
        [SerializeField]
        float targetingDuration;
        [SerializeField]
        Text timerText;

		public bool hasMission()
		{
			return currentState == CivvilianStates.showingMission;
		}

        CivvilianStates currentState;

        public void AcceptMission()
        {
            if (currentState != CivvilianStates.showingMission) return;
            PeopleManager.ReportHumanRitualAnswered();
            missionText.SetActive(false);
            MissionProvider target = PeopleManager.GetRandomTarget(this);
            if (target)
            {
                target.SetAsTarget();
            }
        }

        void SetAsTarget()
        {
            targetDisplay.SetActive(true);
            currentState = CivvilianStates.targeted;
            animator.SetTrigger("Finished Ritual");
            missionText.SetActive(false);
            if (motor)
            {
                motor.Resume();
            }

            StopAllCoroutines();
            endAudio();
            StartCoroutine(CountDown());
        }

        IEnumerator CountDown()
        {
            float startTime = Time.time;
            while (startTime + targetingDuration - Time.time > 0)
            {
                timerText.text = (startTime + targetingDuration - Time.time).ToString("F0");
                yield return null;
                Debug.Log("dropping");
            }
            yield return null;
            targetDisplay.SetActive(false);
            StartCoroutine(GiveMissions());
        }

        [SerializeField]
        AudioClip[] audioClips;
        AudioSource[] audioSources;

        [SerializeField]
        float audioStartPoint;

        void Awake()
        {
            motor = GetComponent<Motor>();
            animator = GetComponent<Animator>();

            if (audioClips.Length > 0)
            {
                audioSources = new AudioSource[audioClips.Length];
                for (int i = 0; i < audioClips.Length; i++)
                {
                    AudioSource newAudio = gameObject.AddComponent<AudioSource>();

                    newAudio.clip = audioClips[i];
                    newAudio.loop = true;
                    newAudio.playOnAwake = false;
                    newAudio.volume = 0.5f;

                    audioSources[i] = newAudio;
                }
            }
        }

        void startAudio()
        {
            foreach (AudioSource audio in audioSources)
            {
                if (audioStartPoint > 0)
                {
                    audio.time = audioStartPoint;
                }

                audio.Play();
            }
        }

        void endAudio()
        {
            foreach (AudioSource audio in audioSources)
            {
                audio.Stop();
            }
        }

        // Use this for initialization
        void Start()
        {

            StartCoroutine(GiveMissions());
            PeopleManager.Register(this);
        }

        IEnumerator GiveMissions()
        {
            while (true)
            {
                currentState = CivvilianStates.idle;
                endAudio();
                animator.SetTrigger("Finished Ritual");
                yield return new WaitForSeconds(Random.Range(minWaitTime, maxWaitTime));
                if (motor)
                {
                    motor.Stop();
                }
                currentState = CivvilianStates.showingMission;
                startAudio();
                animator.SetTrigger("Doing Ritual");
                PeopleManager.ReportHumanRitualStart();
                missionText.SetActive(true);
                yield return new WaitForSeconds(missionDisplayTime);
                missionText.SetActive(false);
                if (motor)
                {
                    motor.Resume();
                }
                PeopleManager.ReportHumanRitualMissed();
            }
        }

        public void IsKilled()
        {
            StopAllCoroutines();
            missionText.SetActive(false);
            PeopleManager.Deregister(this, currentState);
            targetDisplay.SetActive(false);
            animator.SetTrigger("Dead");
        }
    }

    public enum CivvilianStates {idle, showingMission, targeted}
}

interface IAcceptMissions : IEventSystemHandler
{
    void AcceptMission();
}