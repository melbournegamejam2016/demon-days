﻿using UnityEngine;
using System.Collections.Generic;
using People;
using UnityEngine.UI;

public class PeopleManager : MonoBehaviour {

    #region SingletonImplementation
    // Lazy instantiating singleton. Because that never causes any bugs.
    static PeopleManager _instance;
    static PeopleManager instance
    {
        get
        {
            if (_instance) return _instance;
            GameObject clone = new GameObject("PeopleManager");
            _instance = clone.AddComponent<PeopleManager>();
            return _instance;
        }
    }

    void Awake()
    {
        if (_instance)
        {
            Debug.LogError("Two people managers", this);
        }
        _instance = this;
    }

    #endregion

    [SerializeField]
    int maxNoOfPeople = 10;
    [SerializeField]
    List<MissionProvider> people = new List<MissionProvider>();
    [SerializeField]
    PointsMover pointPrefab;

    int points;


    static int noOfInnocentsKilled = 0;
    static int noOfWorshippersKilled = 0;
    static int noOfMarksKilled = 0;
    static int noOfHumanRitualsStarted = 0;
    static int noOfHumanRitualsAnswered = 0;
    static int noOfHumanRitualsMissed = 0;
    static int noOfMarksMissed = 0;

    [SerializeField]
    int killsForDemonHunter = 15;

    [SerializeField]
    DemonHunter demonHunter;

    [SerializeField]
    Text debugText;

    [SerializeField]
    Transform player;

    [SerializeField]
    Text scoreText;



    public static void ReportHumanRitualStart()
    {
        noOfHumanRitualsStarted++;
    }

    public static void ReportHumanRitualAnswered()
    {
        noOfHumanRitualsAnswered++;
    }

    public static void ReportHumanRitualMissed()
    {
        noOfHumanRitualsMissed++;
    }

    public static void ReportMarkMissed()
    {
        noOfMarksMissed++;
    }

    public static bool canSpawnMore {
        get
        {
            return instance.people.Count < instance.maxNoOfPeople;
        }
    }

	public static void Register (MissionProvider person) {
        instance.people.Add(person);
	}

    public static void Deregister(MissionProvider person, CivvilianStates state)
    {
        instance.people.Remove(person);
        switch (state)
        {
            case CivvilianStates.idle:
                noOfInnocentsKilled++;
                if (noOfInnocentsKilled > 1 && noOfInnocentsKilled % instance.killsForDemonHunter == 0)
                {
                    Instantiate<DemonHunter>(instance.demonHunter).player = instance.player;
                }
                break;
            case CivvilianStates.showingMission:
                noOfWorshippersKilled++;
                break;
            case CivvilianStates.targeted:
                noOfMarksKilled++;
                PointsMover clone = (PointsMover)Instantiate(instance.pointPrefab, Camera.main.WorldToScreenPoint(person.transform.position), Quaternion.identity);
                clone.AssignPoints(500, () => instance.points += 500);
                break;
        }
    }

    public static MissionProvider GetRandomTarget (MissionProvider person)
    {
        if (instance.people.Count <= 1) return null;
        MissionProvider returnValue = person;
        while (returnValue == person)
        {
            returnValue = instance.people[Random.Range(0, instance.people.Count)];
        }
        return returnValue;
    }

    void Update()
    {
        if (debugText)
        {
            debugText.text = "No of bystanders killed: " + noOfInnocentsKilled +
                "\nNo of worshippers killed: " + noOfWorshippersKilled +
                "\nNo of marks killed: " + noOfMarksKilled +
                "\nNo of marks missed: " + noOfMarksMissed +
                "\nSummons started: " + noOfHumanRitualsStarted +
                "\nSummons answered: " + noOfHumanRitualsAnswered +
                "\nSummons ignored: " + noOfHumanRitualsMissed;
        }
        scoreText.text = points.ToString();
    }
}
