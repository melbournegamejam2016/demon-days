﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PointsMover : MonoBehaviour {

    [SerializeField]
    Vector2 endPosition;

    [SerializeField]
    float flightTime;

    [SerializeField]
    string displayText;

 	void Start()
    {
        transform.SetParent(GameObject.Find("Main Canvas").transform,true);
        StartCoroutine(Fly());
    }

    public delegate void ScoreCallBack();
    ScoreCallBack scoreCallBack;

    public void AssignPoints(int value, ScoreCallBack callback ) {
        scoreCallBack = callback;
        GetComponent<Text>().text = value + " " + displayText;
    }



	IEnumerator Fly () {
        yield return null;
        float time = 0;
        Vector2 startPosition = ((RectTransform)transform).offsetMin;
        Vector2 offset = ((RectTransform)transform).offsetMax - ((RectTransform)transform).offsetMin;
        while (time < flightTime) {
            yield return null;
            time += Time.deltaTime;
            ((RectTransform)transform).offsetMin = Vector2.Lerp(startPosition, endPosition, time / flightTime);
            ((RectTransform)transform).offsetMax = Vector2.Lerp(startPosition, endPosition, time / flightTime) + offset;
        }
        scoreCallBack();
        Destroy(gameObject);
	}
}
