﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

namespace People {

    [RequireComponent(typeof(SpriteRenderer))]
    public class Damagable : MonoBehaviour, IDamagable, IKillable
    {
        [SerializeField]
        Sprite deadSprite;

        public void DoDamage(float dammage)
        {
            ExecuteEvents.Execute<IKillable>(gameObject, null, (x, y) => x.IsKilled());
        }

        public void IsKilled()
        {
            GetComponent<SpriteRenderer>().sprite = deadSprite;
            gameObject.layer = LayerMask.NameToLayer("Dead");
        }
    }

    public interface IKillable : IEventSystemHandler
    {
        void IsKilled();
    }
}

public interface IDamagable : IEventSystemHandler
{
    void DoDamage(float dammage);
}


