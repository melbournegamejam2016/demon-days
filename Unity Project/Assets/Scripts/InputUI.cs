﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InputUI : MonoBehaviour {
	[SerializeField]
	public Button buttonX;

	[SerializeField]
	public Button buttonY;

	[SerializeField]
	public Button buttonB;

	[SerializeField]
	public Button buttonA;

	[SerializeField]
	public RitualManager ritualManager;

	string defaultActionString;

	GameObject eventSystem;

	void Awake()
	{
		eventSystem = GameObject.Find("EventSystem");
		defaultActionString = buttonX.GetComponentsInChildren<Text> ()[0].text;
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Jump")) {
			buttonA.Select ();
		}

		if (Input.GetButtonDown ("Attack")) {
			buttonB.Select ();
		}

		if (Input.GetButtonDown ("Action")) {
			buttonX.Select ();
		}

		if (Input.GetButtonDown ("Teleport")) {
			buttonY.Select ();
		}

		if (Input.GetButtonUp ("Jump")
			|| Input.GetButtonUp("Action")
			|| Input.GetButtonUp("Attack")
			|| Input.GetButtonUp("Teleport")) {
			eventSystem.GetComponent<UnityEngine.EventSystems.EventSystem>().SetSelectedGameObject(null);
		}

		// Update action label
		Text[] actionText = buttonX.GetComponentsInChildren<Text> ();
		if (ritualManager.activeRitual != null) {
			actionText [0].text = ritualManager.activeRitual.ritualActionName;
			actionText [0].color = Color.yellow;
		} else {
			actionText[0].text = defaultActionString;
			actionText [0].color = Color.white;
		}

		if (!ritualManager.ritualActor.isBelowGround) {
			actionText [0].text = "Accept Mission";
			actionText [0].color = Color.yellow;
		}
	}
}
