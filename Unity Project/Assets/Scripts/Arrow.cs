﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class Arrow : MonoBehaviour {

    [SerializeField]
    float speed = 15;
    [SerializeField]
    float lifeTime;

	// Update is called once per frame
	void Update () {
        lifeTime -= Time.deltaTime;
        if (lifeTime < 0) Destroy(gameObject);
        transform.position = transform.position - transform.right * speed * Time.deltaTime * transform.localScale.x;
        
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        ExecuteEvents.Execute<IEnergyDamage>(other.gameObject, null, (x, y) => x.RemoveAllEnergy());
    }
}

public interface IEnergyDamage : IEventSystemHandler
{
    void RemoveAllEnergy();
}
