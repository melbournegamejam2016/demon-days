﻿using UnityEngine;
using System.Collections;

public class RitualPerformer {
}

public interface IRitualPerformer {
	void OnRitualDidStart (Transform performPosition);

	void OnRitualDidEnd (Transform performPosition, float ritualValue);

	void AddRitualEnergy(float energy);
}