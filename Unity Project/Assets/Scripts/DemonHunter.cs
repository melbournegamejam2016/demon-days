﻿using UnityEngine;
using System.Collections;

public class DemonHunter : MonoBehaviour {

    [SerializeField]
    Vector3 startPosition;
    [SerializeField]
    Vector3 endPosition;
    [SerializeField]
    int noOfArrows;
    [SerializeField]
    float speed = 5;
    public Transform player;
    [SerializeField]
    GameObject arrow;
    [SerializeField]
    GameObject firingPort;

    void Start()
    {
        StartCoroutine(Move());
    }

    IEnumerator Move()
    {
        transform.position = startPosition;
        yield return null;
        float distanceToTravel = (endPosition - startPosition).magnitude;
        Vector3 directionToTravel = (endPosition - startPosition).normalized;
        float remainingDistance = distanceToTravel;
        float distanceJump = distanceToTravel / (noOfArrows + 1);
        float distanceThreshold = distanceToTravel - distanceJump ;
        
        while (remainingDistance > 0.2f)
        {
            transform.position = transform.position + directionToTravel * Time.deltaTime * speed;
            remainingDistance = (transform.position - endPosition).magnitude;
            yield return null;
            if (remainingDistance < distanceThreshold)
            {
                distanceThreshold -= distanceJump;
                Fire();
                while (isFiring) yield return null;
            }
        }
        Destroy(gameObject);
    }

    bool isFiring;

    public void FiringDone()
    {
        isFiring = false;
        GameObject clone = (GameObject)Instantiate(arrow, firingPort.transform.position, firingPort.transform.rotation);
        clone.transform.localScale = transform.localScale;
        transform.localScale = Vector3.one;
    }

    void Fire()
    {
        if (player.position.x - transform.position.x > 0)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        GetComponent<Animator>().SetTrigger("Fire");
        isFiring = true;
    }
}
