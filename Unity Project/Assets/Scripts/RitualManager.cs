﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RitualManager : MonoBehaviour {
	[SerializeField]
	GameManager gameManager;

	[SerializeField]
	public DevilMovement ritualActor;

	[SerializeField]
	InputUI inputUI;

	[SerializeField]
	RitualStation[] rituals;

	[SerializeField]
	Text ritualInfoText;

	[SerializeField]
	Text ritualTimerText;

	[SerializeField]
	GameObject activeRitualMarkerPrefab;
	GameObject activeRitualMarkerObject;
	GameObject acceptMissionMarkerObject;

	// The ritual station the devil is using
	[SerializeField]
	public RitualStation activeRitual;

	[SerializeField]
	float ritualTimerMin;

	[SerializeField]
	float ritualTimerMax;

	// How long do we have to complete a ritual
	[SerializeField]
	float ritualGraceTime = 5.0f;

	// Penalty for failing to perform target ritual
	[SerializeField]
	float ritualFailPenalty = -10.0f;

	// How long to wait before we start with the first ritual
	[SerializeField]
	float firstRitualDelay = 5.0f;

	// Timer - tracks time until next ritual is switched
	float timeTillNextRitual;

	float timeTillRitualFail;

	// Next target ritual
	int nextTargetRitualIndex;

	// The ritual the devil has to perform
	public RitualStation targetRitual;

	[SerializeField]
	Animator ritualHUDAnimator;


	void Awake() {
		foreach (RitualStation ritual in rituals) {
			ritual.ritualManager = this;
		}

		timeTillNextRitual = firstRitualDelay;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeTillNextRitual -= Time.deltaTime;

		if (timeTillNextRitual <= 0.0f) {
			UpdatTargetRitual ();

			timeTillNextRitual = Random.Range (ritualTimerMin, ritualTimerMax);
		}

		// Track time to complete a ritual - fail when exceeded
		if (targetRitual) {
			timeTillRitualFail -= Time.deltaTime;

			if (timeTillRitualFail <= 0.0f) {
				ritualFail ();
			}

			ritualTimerText.text = Mathf.CeilToInt (timeTillRitualFail) + " sec";
		} else {
			ritualTimerText.text = "";
			HideRitualMarker ();
		}
	}

	void UpdatTargetRitual()
	{
		if (gameManager.isGameOver) {
			return;
		}

		if (nextTargetRitualIndex < rituals.Length) {	
			
		} else {
			Debug.Log ("At end of rituals starting again from the first");
			nextTargetRitualIndex = 0;
		}

		targetRitual = rituals [nextTargetRitualIndex++];
		ritualInfoText.text = targetRitual.ritualName;
		ritualInfoText.color = Color.white;
		ritualHUDAnimator.SetTrigger ("Flash");

		// Start ritual fail timer
		timeTillRitualFail = ritualGraceTime;

		if (targetRitual) {
			Vector3 position = targetRitual.transform.position;
			position.y += 2.5f;

			GameObject ritualMarker = GetRitualMarker ();
			ritualMarker.transform.position = position;
		}
	}

	public void SetRitual(RitualStation ritual)
	{
		activeRitual = ritual;
		ritualActor.activeRitual = ritual;
	}

	public void ritualComplete(RitualStation ritual)
	{
		if (ritual == targetRitual) {
			ritualInfoText.text = "Complete!";
			ritualInfoText.color = Color.green;
			targetRitual = null;
			ritualHUDAnimator.SetTrigger ("Fade");
		}
	}

	void ritualFail()
	{
		targetRitual = null;
		ritualInfoText.text = "Failed!";
		ritualInfoText.color = Color.red;

		// Remove penalty energy
		ritualActor.AddRitualEnergy (ritualFailPenalty);
		ritualHUDAnimator.SetTrigger ("Fade");
	}

	GameObject GetRitualMarker()
	{
		if (!activeRitualMarkerObject) {
			activeRitualMarkerObject = Object.Instantiate (activeRitualMarkerPrefab);
		}

		return activeRitualMarkerObject;
	}

	GameObject GetAcceptMissionMarker()
	{
		if (!acceptMissionMarkerObject) {
			acceptMissionMarkerObject = Object.Instantiate (activeRitualMarkerPrefab);
		}

		return acceptMissionMarkerObject;
	}

	public void HideRitualMarker()
	{
		GameObject ritualMarker = GetRitualMarker ();

		ritualMarker.transform.position = new Vector3 (-100.0f, -100.0f, -100.0f);
	}

	public void HideAcceptMissionMarker()
	{
		GameObject acceptMissionMarker = GetAcceptMissionMarker ();

		acceptMissionMarker.transform.position = new Vector3 (-100.0f, -100.0f, -100.0f);
	}
}
