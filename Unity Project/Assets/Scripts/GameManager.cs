﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {

	[SerializeField]
	Text gameTimerText;

	[SerializeField]
	Text gameOverText;

	[SerializeField]
	float timePerRound = 160.0f;

	public bool isGameOver;

	float timeRemaining;

	void Awake()
	{
		timeRemaining = timePerRound;
		gameOverText.gameObject.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
		if (!isGameOver) {
			timeRemaining -= Time.deltaTime;
			gameTimerText.text = Mathf.CeilToInt (timeRemaining).ToString ();

			if (timeRemaining <= 0) {
				gameOverText.gameObject.SetActive(true);
				isGameOver = true;
				gameTimerText.text = "0";
			}
		}
	}
}
