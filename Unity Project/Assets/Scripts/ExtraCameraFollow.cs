﻿using UnityEngine;
using System.Collections;

public class ExtraCameraFollow : MonoBehaviour {

    [SerializeField]
    Transform realTarget;

    [SerializeField]
    float maxX;

    [SerializeField]
    float minX;
	
	// Update is called once per frame
	void Update () {
        float x = Mathf.Clamp(realTarget.position.x, minX, maxX);
        transform.position = new Vector3(x, transform.position.y, transform.position.z);

	}
}
