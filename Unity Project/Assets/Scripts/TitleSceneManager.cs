﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleSceneManager : MonoBehaviour {

    Animator animator;

    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void LoadLevel(string levelToLoad)
    {
        StartCoroutine(LoadLevelCoroutine(levelToLoad));
    }

    public void Exit()
    {
        Application.Quit();
    }


    IEnumerator LoadLevelCoroutine(string levelToLoad)
    {
        yield return null;
        if (animator)
        {
            animator.SetTrigger("Fade");
            while (!animator.GetBool("IsDone")) yield return null;
        }
        SceneManager.LoadScene(levelToLoad);
    }
}
