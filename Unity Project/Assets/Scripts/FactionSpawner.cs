﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace People {
    public class FactionSpawner : MonoBehaviour {

        [SerializeField]
        float minTimeBetweenSpawns = 1;
        [SerializeField]
        float maxTimeBetweenSpawns = 5;
        [SerializeField]
        GameObject peoplePrefab;
        [SerializeField]
        List<RuntimeAnimatorController> animators;

    void Start() {
            StartCoroutine(SpawnSomeMinions());
        }

        IEnumerator SpawnSomeMinions()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(minTimeBetweenSpawns, maxTimeBetweenSpawns));
                if (PeopleManager.canSpawnMore)
                {
                    GameObject clone = (GameObject)Instantiate(peoplePrefab, transform.position, Quaternion.identity);
                    clone.GetComponent<Animator>().runtimeAnimatorController = animators[Random.Range(0, animators.Count)];
                }
            }
        }
    }
}