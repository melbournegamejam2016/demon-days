﻿using UnityEngine;
using System.Collections.Generic;

public class CloudMover : MonoBehaviour {

    List<Transform> clouds = new List<Transform>();
    [SerializeField]
    float speed = 1;

    [SerializeField]
    float jumpDistance = 32;

    [SerializeField]
    float endPostion = 16;

    void Start()
    {
        clouds.AddRange(GetComponentsInChildren<Transform>());
        clouds.Remove(transform);
    }

    void Update()
    {
        foreach(Transform cloud in clouds)
        {
            cloud.position = cloud.position + Vector3.right * Time.deltaTime * speed;
            if (cloud.position.x > endPostion)
            {
                cloud.position = cloud.position - Vector3.right * jumpDistance;
            }
        }
    }
}
